import numpy as np


def gauss(distance):
    return 1.0 / np.sqrt(np.pi) * np.exp(-(np.power(distance, 2) / 2.0))


def inverse_pow(distance):
    return 1.0 / np.power(np.abs(distance), 2)
