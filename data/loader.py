import pandas as pd
import numpy as np

from sklearn.datasets import load_iris
from sklearn.datasets import load_wine

DATASETS = ['iris', 'wine', 'glass', 'seed']


def load_dataset(name):
    # np.atleast_1d(X) make sure that there will not be 0 rank array
    assert name in DATASETS
    X, y = None, None
    if name in ['iris', 'wine']:
        data = load_iris() if name == 'iris' else load_wine()
        X, y = pd.DataFrame(data=data['data']), np.atleast_1d(data['target']).T
    elif name == 'glass':
        df_glass = pd.read_csv('./data/input/glass.data',
                               names=['ID', 'RI', 'Na', 'Mg', 'Al', 'Si', 'K', 'Ca', 'Ba', 'Fe', 'TARGET'])
        X, y = df_glass[df_glass.columns[1:-1]], np.atleast_1d(df_glass[df_glass.columns[-1]]).T
    elif name == 'seed':
        file = open('./data/input/seeds_dataset.txt', mode='r')
        content = [list(map(float, line.split())) for line in file]
        df_seed = pd.DataFrame(content, columns=['Area', 'Perimeter', 'Compactness', 'Kernel length', 'Kernel width',
                                                 'Asymmetry', 'Kernel groove length', 'TARGET'])
        X, y = df_seed[df_seed.columns[:-1]], np.atleast_1d(df_seed[df_seed.columns[-1]]).T
    return X.to_numpy().astype(float), y
