import itertools

from experiment.experiment import run_experiments

DATASETS = ['wine', 'glass', 'seed']

K = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15]
WEIGHTS = ['uniform', 'distance', 'gauss', 'inverse_pow']
ALGORITHM = ['auto']
LEAF_SIZE = [30]
P = [3]
METRIC = ['minkowski', 'euclidean', 'manhattan', 'chebyshev']

parameters = itertools.product(DATASETS, K, WEIGHTS, ALGORITHM, LEAF_SIZE, P, METRIC)

run_experiments(parameters)
