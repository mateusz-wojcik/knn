import csv

import numpy as np

from sklearn.metrics import precision_recall_fscore_support, accuracy_score, confusion_matrix
from sklearn.model_selection import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler

from data.loader import load_dataset
from experiment.metric import inverse_pow, gauss
from experiment.utils import timeit

WEIGHTS = {'gauss': gauss, 'inverse_pow': inverse_pow}
FOLDS = 5


@timeit
def run_experiments(params):
    np.seterr(divide='ignore', invalid='ignore')
    with open('results.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(['dataset', 'K', 'Weights', 'Algorithm', 'Leaf_size', 'P', 'Metric',
                             'Fscore', 'Precision', 'Recall', 'Accuracy',
                             'Fscore std', 'Precision std', 'Recall std', 'Accuracy std'])
        for param in params:
            p = {'k': param[1], 'weights': WEIGHTS[param[2]] if param[2] in WEIGHTS.keys() else param[2],
                 'algorithm': param[3], 'leaf_size': param[4], 'p': param[5], 'metric': param[6]}
            X, y = load_dataset(param[0])
            X = StandardScaler().fit_transform(X)
            cross_validator = StratifiedKFold
            scores = test_crossval(cross_validator, X, y, FOLDS, p)
            fsc_mean, fsc_std = np.round(scores[:, 0].mean(), 4), np.round(scores[:, 0].std(), 4)
            pre_mean, pre_std = np.round(scores[:, 1].mean(), 4), np.round(scores[:, 1].std(), 4)
            rec_mean, rec_std = np.round(scores[:, 2].mean(), 4), np.round(scores[:, 2].std(), 4)
            acc_mean, acc_std = np.round(scores[:, 3].mean(), 4), np.round(scores[:, 3].std(), 4)
            csv_writer.writerow([param[0], p['k'], param[2], p['algorithm'], p['leaf_size'],
                                 p['p'], p['metric'],
                                 fsc_mean, pre_mean, rec_mean, acc_mean,
                                 fsc_std, pre_std, rec_std, acc_std])


def run_experiment(X_train, y_train, X_test, params):
    knn = KNeighborsClassifier(n_neighbors=params['k'], weights=params['weights'], algorithm=params['algorithm'],
                               leaf_size=params['leaf_size'], p=params['p'],
                               metric=params['metric'], metric_params=None, n_jobs=None)
    knn.fit(X_train, y_train)
    prediction = np.atleast_1d(knn.predict(X_test)).T
    return prediction


def calculate_metrics(y_test, y_pred):
    precision, recall, fscore, _ = precision_recall_fscore_support(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    acc = accuracy_score(y_test, y_pred)
    return [fscore, precision, recall, acc]


def test_crossval(cross_validator, X, y, k, params):
    score = []
    kf = cross_validator(n_splits=k, shuffle=True, random_state=42)
    for i, (train_index, test_index) in enumerate(kf.split(X, y)):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        prediction = run_experiment(X_train, y_train, X_test, params)
        results = calculate_metrics(y_test, prediction)
        score.append(results)
    return np.asarray(score)
